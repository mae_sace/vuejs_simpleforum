import Vue from "vue";
import Vuex from "vuex";
import posts, { PostDetails } from "./posts";

Vue.use(Vuex);

type RootState = {
  posts: Array<PostDetails>;
};

export default new Vuex.Store<RootState>({
  modules: { posts }
});
