// Post Type definitions
export type PostDetails = {
  id: number;
  title: string;
  userId: number;
  body: string;
};

// Post states, actions, etc.
const namespaced = true;

const state = {
  posts: [
    { id: 1, title: "Post title" },
    { id: 2, title: "Post title" },
    { id: 3, title: "Post title" },
    { id: 4, title: "Post title" },
    { id: 5, title: "Post title" },
    { id: 6, title: "Post title" },
    { id: 7, title: "Post title" },
    { id: 8, title: "Post title" },
    { id: 9, title: "Post title" }
  ]
};

const mutations = {};

const actions = {};

export default {
  namespaced,
  state,
  mutations,
  actions
};
