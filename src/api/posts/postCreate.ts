import apiUrlPaths from "../constants/apiUrlPaths";
import gql from "graphql-tag";

export const createPostGql = gql`
  mutation {
    createPost(input: $input)
      @rest(type: "Post", method: "POST", path: "${apiUrlPaths.posts}") {
      id
      title
      body
    }
  }
`;
