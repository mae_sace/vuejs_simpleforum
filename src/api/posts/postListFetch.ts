import apiUrlPaths from "../constants/apiUrlPaths";
import gql from "graphql-tag";

export const fetchPostsGql = gql`
  query {
    posts
      @rest(
        type: "Post"
        method: "GET"
        path: "${apiUrlPaths.posts}"
      ) {
      id
      title
    }
  }
`;
