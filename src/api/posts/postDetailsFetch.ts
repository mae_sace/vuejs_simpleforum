import apiUrlPaths from "../constants/apiUrlPaths";
import gql from "graphql-tag";

export const fetchPostDetailsGql = gql`
  query {
    postDetails(postId: $postId)
      @rest(type: "Post", method: "GET", path: "${apiUrlPaths.postDetails}") {
      id
      title
      userId
      body
    }
    comments(postId: $postId)
      @rest(
        type: "Comment"
        method: "GET"
        path: "${apiUrlPaths.postComments}"
      ) {
      id
      name
      email
      body
    }
  }
`;
