import { RestLink } from "apollo-link-rest";
import configurationHelpers from "@/constants/configurationConstants";
const { API_URL } = configurationHelpers;

const restLink = new RestLink({
  uri: `${API_URL}/`,
  endpoints: {
    posts: "/posts"
  }
});

export default restLink;
