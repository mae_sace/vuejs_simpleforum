export default {
  posts: "posts",
  postDetails: "posts/{args.postId}",
  postComments: "posts/{args.postId}/comments"
};
