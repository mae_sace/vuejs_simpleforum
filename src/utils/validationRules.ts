export const requiredField = (fieldName: string) => {
  return [(v: any) => !!v || `${fieldName} is required`];
};
