import Vue from "vue";
import VueRouter from "vue-router";
import PostList from "@/views/PostList.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "PostList",
    component: PostList
  },
  {
    path: "/details/:id",
    name: "PostDetails",
    component: () =>
      import(/* webpackChunkName: "postdetails" */ "../views/PostDetails.vue")
  },
  {
    path: "/create",
    name: "PostCreate",
    component: () =>
      import(/* webpackChunkName: "postcreate" */ "../views/PostCreate.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
