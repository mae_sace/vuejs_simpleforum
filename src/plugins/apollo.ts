import Vue from "vue";
import VueApollo from "vue-apollo";
import { ApolloClient } from "apollo-client";
import { ApolloLink } from "apollo-link";
import { InMemoryCache } from "apollo-cache-inmemory";
import errorHandlerLink from "@/api/apolloClient/errorHandlerLink";
import restLink from "@/api/apolloClient/restApiLink";

Vue.use(VueApollo);

// @ts-ignore
const link = ApolloLink.from([errorHandlerLink, restLink]);

// Cache implementation
const cache = new InMemoryCache();

// Create the apollo client
const apolloClient = new ApolloClient({
  link,
  cache
});

export const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});
